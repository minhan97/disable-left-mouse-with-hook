1. Thông tin
Lê Minh Ân
1512016

2.Các chức năng làm được:
- Vô hiệu hóa chuột trái khi nhấn phím 'L'
- Chương trình sẽ hiển thị message box để thông báo vô hiệu hóa chuột trái.
- Để kích hoạt chuột trái, tắt message box (Enter) và nhấn phím 'L' lần nữa.

3. Link repo: https://minhan97@bitbucket.org/minhan97/disable-left-mouse-with-hook.git

4. Video demo: https://youtu.be/R1JDEW16D9w